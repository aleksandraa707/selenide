import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class SelenideTest3 {
    @Test
    public void selenideTest3(){
        open("https://ithillel.ua/");
        $(By.xpath("//*[@id=\"jvlabelWrap\"]/jdiv/jdiv[2]")).click();
        $(By.xpath("//*[@id=\"scrollbar-container\"]")).should(Condition.appear);
    }

}
