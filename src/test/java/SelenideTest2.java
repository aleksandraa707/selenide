import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class SelenideTest2 {
    @Test
    public void selenideTest2() {
        open("https://ithillel.ua/");
        $(By.xpath("//*[@id=\"signCoursesButton\"]")).click();
        $(By.xpath("//*[@id=\"signCourses\"]/div/form/div/div[2]")).should(Condition.appear);
    }
}
